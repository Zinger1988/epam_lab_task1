const fs = require('fs');
const path = require('path');

const APP_ROOT = path.dirname(require.main.filename);
const FILES_PATH = `${APP_ROOT}/files`;
const ALLOWED_FILE_EXT = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

function isValidExtension(extension, allowedExt){
  return allowedExt.some(item => item === extension);
}

function createFile (req, res, next) {
  const {filename, content: reqContent} = req.body;
  const extension = path.extname(`${FILES_PATH}/${filename}`).slice(1);

  if(!reqContent || !filename){
    const requiredParam = !filename ? 'filename' : 'content';
    res.status(400).send({ "message": `Please specify '${requiredParam}' parameter` });
    return;
  }

  if(!isValidExtension(extension, ALLOWED_FILE_EXT)){
    res.status(400).send({ "message": "Not supported file type. Allowed extensions: .log, .txt, .json, .yaml, .xml, .js" });
    return;
  }

  const fileContent = extension === '.json' ? JSON.stringify(reqContent) : reqContent;

  fs.writeFile(`${FILES_PATH}/${filename}`, fileContent, () => {
    res.status(200).send({ "message": "File created successfully" });
  });
}

function getFiles (req, res, next) {
  fs.readdir(FILES_PATH, (err, files) => {

    if(err) {
      res.status(400).send({ "message": "Client error" });
      return;
    }

    res.status(200).send({
      "message": "Success",
      "files": files
    });
  })
}

const getFile = (req, res, next) => {
  const {filename} = req.params;

  fs.readFile(`${FILES_PATH}/${filename}`, (err, data) => {

    if(err){
      res.status(400).send({ "message": `No file with '${filename}' filename found` });
      return;
    }

    res.status(200).send({
      "message":"Success",
      "filename":filename,
      "content":data.toString(),
      "extension":path.extname(`${FILES_PATH}/${filename}`).slice(1),
      "uploadedDate":fs.statSync(`${FILES_PATH}/${filename}`).birthtime
    });
  })
}

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
}